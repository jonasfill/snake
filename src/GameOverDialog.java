import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameOverDialog extends JFrame {
    private static JDialog dialog;

    public GameOverDialog() {
        dialog = new JDialog(this, "Game over", true);

        // Initialize components
        JLabel label = new JLabel("Game over!");
        JButton button = new JButton("OK");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                Game.timer.restart();
            }
        });

        setLayout(new FlowLayout());
        // Add components
        add(label); add(button);

        setSize(300,150);
        setVisible(true);
    }
}
