import org.w3c.dom.stylesheets.LinkStyle;

import java.awt.*;
import java.util.ArrayList;
import java.util.Queue;

public class Snake {

    private Direction direction;

    private ArrayList<Point> snake;

    // List of old directions
    private ArrayList<Direction> formerDirections;

    public Snake(Direction direction, Point head) {
        // Create initial snake which is only one point "long"
        this.direction = direction;
        this.snake = new ArrayList<>();
        this.snake.add(head);
        formerDirections = new ArrayList<>();
        formerDirections.add(direction);
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
        this.formerDirections.add(direction);
    }

    /**
     * Removes first former direction
     */
    public void removeDirection() {
        formerDirections.remove(0);
    }

    public ArrayList<Point> get() {
        return snake;
    }

    public void set(ArrayList<Point> snake) {
        this.snake = snake;
    }

    /**
     * Append new tail part to snake
     */
    public void append() {
        int x, y;
        switch (this.direction) {
            case DOWN:
                x = snake.get(snake.size() - 1).x;
                y = snake.get(snake.size() - 1).y - 1;
                break;
            case UP:
                x = snake.get(snake.size() - 1).x;
                y = snake.get(snake.size() - 1).y + 1;
                break;
            case RIGHT:
                x = snake.get(snake.size() - 1).x - 1;
                y = snake.get(snake.size() - 1).y;
                break;
            case LEFT:
                x = snake.get(snake.size() - 1).x + 1;
                y = snake.get(snake.size() - 1).y;
                break;
            default:
                x = snake.get(snake.size() - 1).x;
                y = snake.get(snake.size() - 1).y;
        }
        snake.add(new Point(x, y));
    }

    /**
     * Update position of snake
     */
    public void update(java.util.List<Integer> borders) {
        // Keeps track of lower border
        int lowerBorder = 0;
        if (borders.isEmpty()) {
            // If there are no direction segments, update whole snake with same direction setting
            updateInner(0, snake.size(), direction);
        }
        else {
            // keep separate index for directions list
            int directionIndex = formerDirections.size() - 1;
            for (Integer border : borders) {
                // Only call update function if snake segment contains at least one point
                if (lowerBorder < border) {
                    updateInner(lowerBorder, border, formerDirections.get(directionIndex));
                    lowerBorder = border;
                    directionIndex--;
                }
            }
            if (lowerBorder < snake.size()) {
                updateInner(lowerBorder, snake.size(), formerDirections.get(directionIndex));
            }
        }
    }

    public void updateInner(int lower, int upper, Direction direction) {
        Point head = snake.get(lower);
        switch (direction) {
            case DOWN:
                for (int i = 0; i < upper - lower; i++) {
                    snake.set(lower + i, new Point(head.x, (head.y - i) + 1));
                }
                break;
            case UP:
                for (int i = 0; i < upper - lower; i++) {
                    snake.set(lower + i, new Point(head.x, (head.y + i) - 1 ));
                }
                break;
            case RIGHT:
                for (int i = 0; i < upper - lower; i++) {
                    snake.set(lower + i, new Point((head.x - i) + 1, head.y));
                }
                break;
            case LEFT:
                for (int i = 0; i < upper - lower; i++) {
                    snake.set(lower + i, new Point((head.x + i) - 1, head.y));
                }
                break;
            default:
        }
    }

    /**
     * Checks if the snake has 'biten' into its tail
     * @return
     */
    public boolean checkGameOver() {
        // Check if any tail point equals the head of the snake
        ArrayList<Point> tail = new ArrayList<>(snake.subList(1, snake.size()));
        for (Point point : tail) {
            if (point.x == snake.get(0).x && point.y == snake.get(0).y) {
                return true;
            }
        }
        // Check if the head overflows the grid
        if (snake.get(0).x < 0 || snake.get(0).x >= Grid.WIDTH / Grid.GRID_SIZE ||
        snake.get(0).y < 0 || snake.get(0).y >= Grid.HEIGHT / Grid.GRID_SIZE) {
            return true;
        }
        return false;
    }
}
