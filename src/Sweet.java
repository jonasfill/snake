import java.awt.*;
import java.util.Random;

public class Sweet {
    private Point sweet;

    public Sweet() {
        // Place sweet at random position
        Random x = new Random();
        Random y = new Random();
        this.sweet = new Point(x.nextInt(Grid.WIDTH / Grid.GRID_SIZE), y.nextInt(Grid.HEIGHT / Grid.GRID_SIZE));
    }

    public Point get() {
        return sweet;
    }

    public void reset() {
        Random x = new Random();
        Random y = new Random();
        this.sweet = new Point(x.nextInt(Grid.WIDTH - Grid.GRID_SIZE), y.nextInt(Grid.HEIGHT - Grid.GRID_SIZE));
    }
}
