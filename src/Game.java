import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Game extends JFrame implements ActionListener, KeyListener {

    // dimensions of grid
    public final static int WIDTH = 400;
    public final static int HEIGHT = 450;

    // refreshing frequency
    private static int FREQUENCY = 10;

    // current game score
    private static int score = 0;

    // initialize grid
    private Grid grid;

    // indicates current score
    private ScoreLabel scoreIndicator;

    // separates snake direction segments
    private ArrayList<Integer> borders;

    private int counter = 0;

    public static Timer timer;

    public Game() {
        grid = new Grid();
        scoreIndicator = new ScoreLabel();
        borders = new ArrayList<>();

        setVisible(true);
        setSize(WIDTH,HEIGHT+20);

        initGame();

        addKeyListener(this);
        // Start timer
        timer = new Timer(20, this);
        timer.start();
    }

    public void initGame() {
        // Insert grid
        add(scoreIndicator, BorderLayout.PAGE_END); add(grid);
        // Insert point
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void resetGame() {
        FREQUENCY = 10;
        timer.stop();
        grid.setSnake(new Snake(Direction.DOWN, new Point(0,0)));
        borders.clear();
        scoreIndicator.setScore(0);
        score = 0;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            grid.repaint();
            // Implement movement of snake
            if (counter % FREQUENCY == 0) {
                // Snake has "catched" the sweet
                if (grid.getSnake().get().get(0).x == grid.getSweet().get().x &&
                        grid.getSnake().get().get(0).y == grid.getSweet().get().y) {
                    // Put in new sweet, append block to snake and update score
                    grid.getSnake().append();
                    grid.setSweet(new Sweet());
                    // Increase speed every five points
                    if (score != 0 && score % 5 == 0 && FREQUENCY > 1) {
                        FREQUENCY--;
                    }
                    // Update score according to current level
                    score += 1;
                    // Update score
                    scoreIndicator.setScore(score);
                }
                grid.getSnake().update(borders);
                // Increment direction segment borders
                for (int i = 0; i < borders.size(); i++) {
                    // If current border exceeds snake length, remove it from the list
                    if (borders.get(i) >= grid.getSnake().get().size()) {
                        borders.remove((int) i);
                        // Remove first direction from former directions
                        grid.getSnake().removeDirection();
                    } else {
                        borders.set(i, borders.get(i) + 1);
                    }
                }
                // Checks if the game is over
                if (grid.getSnake().checkGameOver()) {
                    new GameOverDialog();
                    // Initialize new game
                    resetGame();
                    initGame();
                }
            }
            counter++;

        } catch (IndexOutOfBoundsException ex) {

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        try {
            boolean validKey = true;
            // Change direction of snake according to pressed key
            int keyPressed = e.getKeyCode();
            switch (keyPressed) {
                case KeyEvent.VK_DOWN: {
                    grid.getSnake().setDirection(Direction.DOWN);
                    break;
                }
                case KeyEvent.VK_UP: {
                    grid.getSnake().setDirection(Direction.UP);
                    break;
                }
                case KeyEvent.VK_LEFT: {
                    grid.getSnake().setDirection(Direction.LEFT);
                    break;
                }
                case KeyEvent.VK_RIGHT: {
                    grid.getSnake().setDirection(Direction.RIGHT);
                    break;
                }
                default:
                    validKey = false;
            }
            if (validKey) {
                // add border to end of list
                borders.add(0, 1);
            }
        }
        catch (IndexOutOfBoundsException i) {

        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public static void main(String[] args) {
        new Game();
    }

}
