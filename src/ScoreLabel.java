import javax.swing.*;
import java.awt.*;

public class ScoreLabel extends JPanel {

    private JLabel label = new JLabel("Score:");

    private JLabel score = new JLabel("0");

    public ScoreLabel() {
        setPreferredSize(new Dimension(400, 50));
        label.setHorizontalAlignment(JLabel.CENTER); score.setHorizontalAlignment(JLabel.CENTER);
        add(label, BorderLayout.LINE_START);
        add(score, BorderLayout.LINE_END);
    }

    /**
     * Update score
     * @param score
     */
    public void setScore(int score) {
        this.score.setText(Integer.toString(score));
    }
}
