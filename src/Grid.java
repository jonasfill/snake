import javax.swing.*;
import java.awt.*;

public class Grid extends JPanel {

    public static int WIDTH = 400;

    public static int HEIGHT = 400;

    public final static int GRID_SIZE = 20;

    // insert snake into game
    private Snake snake;

    // insert sweet into game
    private Sweet sweet;

    public Grid() {
        snake = new Snake(Direction.DOWN, new Point(0,0));
        sweet = new Sweet();
    }
    public void paintComponent(Graphics g) {
        super.paintComponents(g);

        g.setColor(Color.BLACK);
        g.fillRect(0,0,WIDTH,HEIGHT);

        g.setColor(Color.BLUE);
        // Fill all points belonging to the snake
        if (snake != null) {
            for (Point point : snake.get()) {
                g.fill3DRect(point.x * GRID_SIZE, point.y * GRID_SIZE, GRID_SIZE, GRID_SIZE, true);
            }
        }

        if (sweet != null) {
            // Fill points belonging to sweet
            g.setColor(Color.RED);
            g.fillOval(sweet.get().x * GRID_SIZE, sweet.get().y * GRID_SIZE, GRID_SIZE, GRID_SIZE);
        }
    }

    public Snake getSnake() {
        return snake;
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
    }

    public Sweet getSweet() {
        return sweet;
    }

    public void setSweet(Sweet sweet) {
        this.sweet = sweet;
    }
}
